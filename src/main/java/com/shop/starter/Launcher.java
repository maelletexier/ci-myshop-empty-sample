package com.shop.starter;

import java.util.ArrayList;
import java.util.List;

import com.ci.myShop.controller.Shop;
import com.ci.myShop.model.Item;

public class Launcher {
	public static void main(String[] args) {
		
		List<Item> items = new ArrayList<Item>();
		Item item1 = new Item("item1", 1, 20, 60);
		Item item2 = new Item("item2", 2, 80, 300);
		Item item11 = new Item("item11",11,30,400);
		Item item22 = new Item("item22",22,40,200);
		
		items.add(item1);
		items.add(item2);
		
		Shop shop = new Shop(items);
	
		shop.sell("item1");
		shop.sell("item2");
		shop.updateCash();
		System.out.println(shop.updateCash());

		shop.buy(item1);
		shop.updateCash();
		
		System.out.println(shop.updateCash());
	}

}
