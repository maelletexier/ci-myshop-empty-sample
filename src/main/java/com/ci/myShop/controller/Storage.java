package com.ci.myShop.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import com.ci.myShop.model.Item;

public class Storage {
	 Map<String, Item> itemMap ;

public Storage(List<Item> items) {
		super();
		itemMap = new HashMap<String, Item>();          
		for(int i=0;i<items.size();i++){             
			Item item = items.get(i);              
			itemMap.put(item.getName(), item);};
	}

public void addItem(Item obj) {
	
	itemMap.put(obj.getName(), obj);
}

public Item getItem(String name) {
	
	Item item = itemMap.get(name);
	return item;
}
public Map<String, Item> getProduct(){
	return itemMap;
}
}
