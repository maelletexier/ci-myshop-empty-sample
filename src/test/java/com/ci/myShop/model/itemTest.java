package com.ci.myShop.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class itemTest {

	@Test
	public void itemCreateTest () {
		Item item = new Item();
		assertNotEquals(null,item);
		
	}
	@Test
	public void itemChangeTest () {
		Item item = new Item("Name", 1, 25, 50);
		item.setName("objectItem");
		assertEquals("objectItem", item.getName());
	}
	@Test
	public void itemGetNameTest () {
		Item item = new Item("Name", 1, 25, 50);
		assertEquals("Name", item.getName());
		
	}
}
